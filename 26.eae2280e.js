exports.ids = [26];
exports.modules = {

/***/ 960:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "taro_moveable_area_core", function() { return MoveableArea; });
/* harmony import */ var _core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(176);

var MoveableArea = /** @class */ (function () {
    function MoveableArea(hostRef) {
        Object(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* r */ "g"])(this, hostRef);
    }
    MoveableArea.prototype.componentDidLoad = function () {
        console.error('H5 暂不支持 MoveableArea 组件！');
    };
    MoveableArea.prototype.render = function () {
        return (Object(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* h */ "f"])(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* H */ "a"], null));
    };
    return MoveableArea;
}());



/***/ })

};;