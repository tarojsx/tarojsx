exports.ids = [18];
exports.modules = {

/***/ 950:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "taro_camera_core", function() { return Camera; });
/* harmony import */ var _core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(176);

var Camera = /** @class */ (function () {
    function Camera(hostRef) {
        Object(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* r */ "g"])(this, hostRef);
    }
    Camera.prototype.componentDidLoad = function () {
        console.error('H5 暂不支持 Camera 组件！');
    };
    Camera.prototype.render = function () {
        return (Object(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* h */ "f"])(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* H */ "a"], null));
    };
    return Camera;
}());



/***/ })

};;