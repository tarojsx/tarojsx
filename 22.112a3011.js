exports.ids = [22];
exports.modules = {

/***/ 954:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "taro_cover_view_core", function() { return CoverView; });
/* harmony import */ var _core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(176);

var CoverView = /** @class */ (function () {
    function CoverView(hostRef) {
        Object(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* r */ "g"])(this, hostRef);
    }
    CoverView.prototype.componentDidLoad = function () {
        console.error('H5 暂不支持 CoverView 组件！');
    };
    CoverView.prototype.render = function () {
        return (Object(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* h */ "f"])(_core_42efd82a_js__WEBPACK_IMPORTED_MODULE_0__[/* H */ "a"], null));
    };
    return CoverView;
}());



/***/ })

};;